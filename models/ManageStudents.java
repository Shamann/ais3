package models;

import java.sql.ResultSet;
import java.sql.SQLException;

import dataObjects.StudentTable;
import database.Configuration;
import database.DatabaseConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ManageStudents {
	
	public ObservableList<StudentTable> loadStudentsToTable() throws SQLException {
		ObservableList<StudentTable> output = FXCollections.observableArrayList();
		
		String query = "SELECT studentID, meno, priezvisko, rodne_c, dat_nar, semester, rocnik, ustav, mail FROM `student`";
		ResultSet studentResult = DatabaseConnection.getMe().makeDatabaseCall(query);
		while (studentResult.next()) {
			output.add(new StudentTable(studentResult.getInt("studentID"),
					studentResult.getString("meno"),
					studentResult.getString("priezvisko"), 
					studentResult.getString("rodne_c"), 
					studentResult.getString("dat_nar"), 
					studentResult.getString("ustav"), 
					studentResult.getInt("semester"), 
					studentResult.getInt("rocnik"),
					studentResult.getString("mail")));
		}
		return output;
	}
	
	public void deleteStudent(String name, String surname, String birthNum) throws SQLException {
		String query = "DELETE FROM `student` where meno = '" + name
				+ "' and priezvisko = '" + surname + "' and rodne_c = '"
				+ birthNum + "';"; 
		
		DatabaseConnection.getMe().makeDatabaseUpdate(query);

	}

	public void insertStudent(String name, String surname, String birthnum,
			String birthdate, String period, String year, String faculty, String email) throws SQLException {
		int semester = Integer.valueOf(period);
		int rocnik = Integer.valueOf(year);
		String login = "00" + surname.toLowerCase() + "14";
		String passwd = Configuration.getRandomPass();
		String date = birthdate.replace("-", "");
		date.replace(".", "");
		
				//TODO login sufixes
		String query = "INSERT INTO `student` SET login='" + login
				+ "', passwd='" + passwd + "', meno='" + name + "', priezvisko='"
				+ surname + "', rodne_c='" + birthnum + "', dat_nar='"
				+ date + "', semester=" + semester + ", rocnik=" + rocnik
				+ ", ustav='" + faculty + "', mail='"
				+ email + "';";
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		
	}

	public void updateStudent(int studentID, String name, String surname, String birthnum,
			String birthdate, String period, String year, String faculty, String email, String passwd) throws SQLException {
		int semester = Integer.valueOf(period);
		int rocnik = Integer.valueOf(year);
		String date = birthdate.replace("-", "");
		date.replace(".", "");
		
		String query = "UPDATE `student` SET ";
		if (passwd.length() >= 8) {
			query += "passwd='" + passwd + "', meno='" + name + "', priezvisko='"
					+ surname + "', rodne_c='" + birthnum + "', dat_nar='"
					+ date + "', semester=" + semester + ", rocnik=" + rocnik
					+ ", ustav='" + faculty + "', mail='"
					+ email + "' WHERE studentID=" + studentID + ";";
		} else {
			query += "meno='" + name + "', priezvisko='"
					+ surname + "', rodne_c='" + birthnum + "', dat_nar='"
					+ date + "', semester=" + semester + ", rocnik=" + rocnik
					+ ", ustav='" + faculty + "', mail='"
					+ email + "' WHERE studentID=" + studentID + ";";
		}
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		
	}
	/**
	 * 
	 * @param subjectID
	 * @return observable list of all students attending this subject
	 * @throws SQLException
	 */
	public ObservableList<StudentTable> getStudentsFromSubject(String subjectID) throws SQLException {
		ObservableList<StudentTable> output = FXCollections.observableArrayList();
		String query = "SELECT s.studentID, s.meno, s.priezvisko, s.rocnik, s.semester, zp.body_zapocet, zp.body_skuska, zp.spravil_predmet "
				+ "FROM student AS s "
				+ "JOIN zapisany_predmet AS zp ON zp.studentID = s.studentID "
				+ "JOIN predmet AS p ON p.predmetID = zp.predmetID "
				+ "WHERE p.predmetID =" + subjectID 
				+ " GROUP BY s.studentID"
				+ " ORDER BY s.meno;";
		
		ResultSet studentFrom = DatabaseConnection.getMe().makeDatabaseCall(query);
		
		while (studentFrom.next()) {
			output.add(new StudentTable(studentFrom.getInt("studentID"),
					studentFrom.getString("meno") + " " +
					studentFrom.getString("priezvisko"),
					studentFrom.getInt("rocnik"),
					studentFrom.getInt("semester"),
					studentFrom.getInt("body_zapocet"),
					studentFrom.getInt("body_skuska"),
					studentFrom.getInt("spravil_predmet")
					));
		}
		
		return output;
	}

	public boolean addYearPoints(String oldValue, String addNewValue, String subjectID, String studentID) throws SQLException {
		
		if (addNewValue.length() > 0 && subjectID.length() > 0 && studentID.length() > 0) {
			String query = "UPDATE zapisany_predmet SET body_zapocet =" + oldValue +
					" + " + addNewValue + " WHERE predmetID =" + subjectID
					+ " AND studentID =" + studentID + ";";
			
			DatabaseConnection.getMe().makeDatabaseUpdate(query);
			
			return true;
		} else {
			return false;
		}
	}
	
	public boolean addExamPoints(String oldValue, String addNewValue, String subjectID, String studentID, String succ) throws SQLException {
		String spravil = "0";
		if (succ.length() > 0 && Integer.valueOf(succ) > 0) {
			spravil = "1";
		}
		
		if (addNewValue.length() > 0 && subjectID.length() > 0 && studentID.length() > 0) {
			String query = "UPDATE zapisany_predmet SET body_skuska =" 
					+ addNewValue + ", spravil_predmet= " + spravil + " WHERE predmetID =" + subjectID
					+ " AND studentID =" + studentID + ";";
			
			DatabaseConnection.getMe().makeDatabaseUpdate(query);
			
			return true;
		} else if (spravil.equalsIgnoreCase("1")){
			String query = "UPDATE zapisany_predmet SET spravil_predmet= " + spravil + " WHERE predmetID =" + subjectID
					+ " AND studentID =" + studentID + ";";
			DatabaseConnection.getMe().makeDatabaseUpdate(query);
			return true;
		}
		return false;
	}
	
}
