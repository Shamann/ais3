package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import dataObjects.TeacherTable;
import database.Configuration;
import database.DatabaseConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ManageTeachers {
	
	public ObservableList<TeacherTable> loadTeachersToTable() throws SQLException {
		ObservableList<TeacherTable> output = FXCollections.observableArrayList();
		
		String query = "SELECT ucitelID, meno, priezvisko, od_dna, mail FROM `ucitel`";
		ResultSet teacherResult = DatabaseConnection.getMe().makeDatabaseCall(query);
		while (teacherResult.next()) {
			output.add(new TeacherTable(teacherResult.getInt("ucitelID"),
					teacherResult.getString("meno"),
					teacherResult.getString("priezvisko"), 
					teacherResult.getString("od_dna"),
					teacherResult.getString("mail")));
		}
		return output;
	}
	
	public void deleteTeacher(String name, String surname, int teacherID) throws SQLException {
		String query = "DELETE FROM `ucitel` WHERE ucitelID=" + teacherID + ";";
		DatabaseConnection.getMe().makeDatabaseUpdate(query);

	}

	public String insertTeacher(String name, String surname, String teacherSince, String email) throws SQLException {
		
		String login = surname.toLowerCase();
		String passwd = Configuration.getRandomPass();
		String date = teacherSince.replace("-", "");
		date.replace(".", "");
				//TODO login sufixes
		String query = "INSERT INTO `ucitel` SET login='" + login
				+ "', passwd='" + passwd + "', meno='" + name + "', priezvisko='"
				+ surname + "', od_dna=" + date + ", mail='"
				+ email + "';";
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		return passwd;
	}

	public void updateTeacher(int teacherID, String name, String surname, String teacherSince, String email, String passwd) throws SQLException {

		String date = teacherSince.replace("-", "");
		date.replace(".", "");
		String query = "UPDATE `ucitel` SET ";
		if (passwd.length() >= 8) {
			query += "passwd='" + passwd + "', meno='" + name + "', priezvisko='"
					+ surname + "', od_dna=" + date + ", mail='"
					+ email + "' WHERE ucitelID=" + teacherID + ";";
		} else {
			query += "meno='" + name + "', priezvisko='"
					+ surname + "', od_dna='" + teacherSince + "', mail='"
					+ email + "' WHERE ucitelID=" + teacherID + ";";
		}
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		
	}

	public void dropLectoring(String predmetID) throws SQLException {
		
		String query = "UPDATE `predmet` SET `prednasajuciID` = null WHERE `predmet`.`predmetID` =" + predmetID + ";";
		
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		
	}

	public void becomeLector(String predmetID, int ucitelID) throws SQLException {
		String queryS = "SELECT prednasajuciID FROM prednasajuci WHERE ucitelID =" + ucitelID + ";";

		
		Statement stmt = DatabaseConnection.getMe().getConnection().createStatement();
		
		DatabaseConnection.getMe().getConnection().setAutoCommit(false);

		ResultSet rsS = stmt.executeQuery(queryS);
		
		if (rsS.next()) {
			
			int prednasajuciID = rsS.getInt("prednasajuciID");
			String queryU1 = "UPDATE `predmet` SET `prednasajuciID` = '" + prednasajuciID + "' WHERE `predmetID` =" + predmetID + ";";
			stmt.executeUpdate(queryU1);
			
		} else {
			int day = Calendar.DAY_OF_MONTH;
			int month = Calendar.MONTH;
			int year = Calendar.YEAR;
			String date = year + "-" + month +"-"+day;
			String queryI1 = "INSERT INTO prednasajuci SET od_dna='" + date + "', ucitelID=" + ucitelID + ";";
			stmt.executeUpdate(queryI1, Statement.RETURN_GENERATED_KEYS);
			ResultSet insertResult = stmt.getGeneratedKeys();
			
			if (insertResult.next()) {
				int prednasajuciID = insertResult.getInt(1);
				String queryI2 = "UPDATE predmet SET prednasajuciID = '" + prednasajuciID + "' WHERE predmetID =" + predmetID + ";";
				
				stmt.executeUpdate(queryI2);
			}
		}
		
		DatabaseConnection.getMe().getConnection().commit();
		
	}

	public void createNewSubject(String name, String credits, String about, int userID) throws SQLException {
		String query = "INSERT INTO predmet SET meno='" + name
				+ "', kredity='" + credits 
				+ "', popis='" + about + "';";
		
		Statement stmt = DatabaseConnection.getMe().getConnection().createStatement();
		
		DatabaseConnection.getMe().getConnection().setAutoCommit(false);
		
		stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
		ResultSet insertResult = stmt.getGeneratedKeys();
		
		if (insertResult.next()) {
			int subjectID = insertResult.getInt(1);
			becomeLector(subjectID + "", userID);
		}
		
	}
	
	
}
