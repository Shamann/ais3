package models;

import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import database.DatabaseConnection;
import user.UserInfo;

public class StudentDetailsManager {

	/**
	 * 
	 * @param studentID
	 * @return id, name, year, period, faculty, subjCount, subjDone, credits
	 * @throws SQLException
	 */
	public String[] getStatistics(int studentID) throws SQLException {
		String[] output = new String[8];
		output[0] = UserInfo.getUser().getUserID() + "";
		output[1] = UserInfo.getUser().getName() + " " + UserInfo.getUser().getSurname();
		
		String query = "SELECT s.semester, s.rocnik, s.ustav, COUNT(*) AS predmety FROM student AS s "
				+ "JOIN zapisany_predmet AS zp ON zp.studentID = s.studentID "
				+ "WHERE s.studentID = " + studentID + ";";
		
		ResultSet firstInfo = DatabaseConnection.getMe().makeDatabaseCall(query);
		if (firstInfo.next()) {
			output[2] = firstInfo.getInt("rocnik") + "";
			output[3] = firstInfo.getInt("semester") + "";
			output[4] = firstInfo.getString("ustav");
			output[5] = firstInfo.getInt("predmety") + "";
		} else {
			output[2] = "";
			output[3] = "";
			output[4] = "";
			output[5] = "";
		}
		query = "SELECT  SUM(p.kredity) AS kredity, COUNT(*) AS spravene FROM student AS s "
				+ "JOIN zapisany_predmet AS zp ON zp.studentID = s.studentID "
				+ "JOIN predmet AS p ON zp.predmetID = p.predmetID "
				+ "WHERE s.studentID = "
				+ studentID +" AND zp.spravil_predmet = 1;";
		
		firstInfo = DatabaseConnection.getMe().makeDatabaseCall(query);
		if (firstInfo.next()) {
			output[6] = firstInfo.getInt("spravene") + "";
			output[7] = firstInfo.getInt("kredity") + "";
		} else {
			output[6] = "";
			output[7] = "";
		}
		return output;
	}
	
	public ObservableList<String> getStudentSubjects(String student) throws SQLException {
		ObservableList<String> output = FXCollections.observableArrayList();
		String[] tmp = student.split(" ");
		String ID = tmp[0];
		if (ID.length() > 0) {
			String query = "SELECT p.predmetID, p.meno, p.kredity, zp.spravil_predmet FROM zapisany_predmet AS zp "
					+ "INNER JOIN predmet AS p ON zp.predmetID = p.predmetID "
					+ "INNER JOIN student AS s ON zp.studentID = s.studentID "
					+ "WHERE s.studentID ="
					+ ID + ";";
			
			ResultSet studentSubjects = DatabaseConnection.getMe().makeDatabaseCall(query);
			
			while (studentSubjects.next()) {
				String tmp2 = studentSubjects.getInt("predmetID") + " - " + studentSubjects.getString("meno") + " k: " + studentSubjects.getInt("kredity") + "   •••    Succ: ";

				if (studentSubjects.getInt("spravil_predmet") == 1) {
					tmp2 += "A";
				} else {
					tmp2 += "N";
				}
						
				output.add(tmp2);
			}
		}
		return output;
	}

}
