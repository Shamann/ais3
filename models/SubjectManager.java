package models;

import java.sql.ResultSet;
import java.sql.SQLException;

import dataObjects.SubjectList;
import database.DatabaseConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class SubjectManager {

	public ObservableList<String> getSubjectStatistics() throws SQLException {
		ObservableList<String> output = FXCollections.observableArrayList();
		
		String query = "SELECT p.predmetID, p.meno, COUNT(*) AS studentiPredmetu from predmet AS p "
				+ "JOIN zapisany_predmet AS zp ON zp.predmetID = p.predmetID "
				+ "GROUP BY p.predmetID "
				+ "ORDER BY p.meno;";
		
		ResultSet subjectStats = DatabaseConnection.getMe().makeDatabaseCall(query);
		
		while (subjectStats.next()) {
			
			output.add(subjectStats.getInt("predmetID") + " - " + subjectStats.getString("meno") + "    •••    Subsrc.: " + subjectStats.getInt("studentiPredmetu"));
		}
		
		return output;
	}
	
	public ObservableList<String> getStudents(String name) throws SQLException {
		ObservableList<String> output =  FXCollections.observableArrayList();
		
		if (name.length() > 0) {
			String query = "SELECT studentID, meno, priezvisko FROM `student` "
					+ "WHERE LOWER(meno) LIKE '" + name.toLowerCase() 
					+ "%' or LOWER(priezvisko) LIKE'" + name.toLowerCase() + "%' ORDER BY meno;";
			
			ResultSet foundNames = DatabaseConnection.getMe().makeDatabaseCall(query);
			
			while (foundNames.next()) {
				output.add(foundNames.getInt("studentID") + " - " + foundNames.getString("meno") + " " + foundNames.getString("priezvisko"));
			}
		}
		return output;
	}

	public ObservableList<String> getStudentSubjects(String student) throws SQLException {
		ObservableList<String> output = FXCollections.observableArrayList();
		String[] tmp = student.split(" ");
		String ID = tmp[0];
		if (ID.length() > 0) {
			String query = "SELECT p.predmetID, p.meno, p.kredity "
					+ "FROM zapisany_predmet AS zp "
					+ "INNER JOIN predmet AS p ON zp.predmetID = p.predmetID "
					+ "INNER JOIN student AS s ON zp.studentID = s.studentID "
					+ "WHERE s.studentID ="
					+ ID + " ORDER BY p.meno;";
			
			ResultSet studentSubjects = DatabaseConnection.getMe().makeDatabaseCall(query);
			
			while (studentSubjects.next()) {
				output.add(studentSubjects.getInt("predmetID") + " - " + studentSubjects.getString("meno") + " k: " + studentSubjects.getInt("kredity"));
			}
		}
		return output;
	}
	
	public ObservableList<String> getStudentSubjectStats(String student) throws SQLException {
		ObservableList<String> output = FXCollections.observableArrayList();
		String[] tmp = student.split(" ");
		String ID = tmp[0];
		if (ID.length() > 0) {
			String query = "SELECT p.predmetID, p.meno, p.kredity, zp.body_skuska, zp.body_zapocet "
					+ "FROM zapisany_predmet AS zp "
					+ "INNER JOIN predmet AS p ON zp.predmetID = p.predmetID "
					+ "INNER JOIN student AS s ON zp.studentID = s.studentID "
					+ "WHERE s.studentID ="
					+ ID + " ORDER BY p.meno;";
			
			ResultSet studentSubjects = DatabaseConnection.getMe().makeDatabaseCall(query);
			
			while (studentSubjects.next()) {
				output.add(studentSubjects.getInt("predmetID") + " - "
						+ studentSubjects.getString("meno") + " k: "
						+ studentSubjects.getInt("kredity")
						+ "   \t•••••   \tTests: " + studentSubjects.getInt("body_zapocet")
						+ "pts.   \tExam: " + studentSubjects.getInt("body_skuska") + "pts.");
			}
		}
		return output;
	}

	public ObservableList<String> getAllSubjectsForStudent() throws SQLException {
		
		ObservableList<String> output = FXCollections.observableArrayList();
		
			String query = "SELECT p.predmetID, p.meno, p.kredity FROM predmet AS p;";
			
			ResultSet otherSubjects = DatabaseConnection.getMe().makeDatabaseCall(query);
			
			while (otherSubjects.next()) {
				output.add(otherSubjects.getInt("predmetID") + " - " + otherSubjects.getString("meno") + " k: " + otherSubjects.getInt("kredity"));
			}
		return output;
	}
	/**
	 * Adds new subject for student with studentId
	 * @param studentId
	 * @param subjectID
	 * @param year
	 * @param semest
	 * @throws SQLException
	 */
	public void addSubject(String studentId, String subjectID, String year, String semest) throws SQLException {
		if (subjectID.length() > 0 && year.length() > 0 && semest.length() > 0) {
			String query = "INSERT INTO zapisany_predmet SET rok =" + year
					+ ", semester = " + semest + ", studentID = " + studentId
					+ ", predmetID = " + subjectID + ";";
			
			DatabaseConnection.getMe().makeDatabaseUpdate(query);
		}
		
	}

	public void deleteSubject(String studentID, String subjectID) throws SQLException {
		if (subjectID.length() > 0) {
			String query = "DELETE FROM zapisany_predmet WHERE studentID = "
					+ studentID + " and predmetID = " + subjectID + ";";
			
			DatabaseConnection.getMe().makeDatabaseUpdate(query);
			
		}
		
	}

	public ObservableList<SubjectList> getAllSubjects() throws SQLException {
		ObservableList<SubjectList> output = FXCollections.observableArrayList();
		String query = "SELECT p.*, u.ucitelID, u.meno as ucitelmeno, u.priezvisko FROM predmet AS p "
				+ "LEFT JOIN prednasajuci AS pr ON p.prednasajuciID = pr.prednasajuciID "
				+ "LEFT JOIN ucitel AS u ON pr.ucitelID = u.ucitelID ORDER BY p.meno; ";
		
		ResultSet subjRes = DatabaseConnection.getMe().makeDatabaseCall(query);
		
		while(subjRes.next()) {
			String ucitelmeno = subjRes.getString("ucitelmeno") + " " + subjRes.getString("priezvisko");
			int ucitelID = subjRes.getInt("ucitelID");
			if (ucitelmeno.equalsIgnoreCase("null null")) {
				ucitelmeno = "";
				ucitelID = -1;
			}
			output.add(new SubjectList(subjRes.getInt("predmetID"), 
					subjRes.getString("meno"), 
					subjRes.getInt("kredity"), 
					ucitelID, 
					ucitelmeno, 
					subjRes.getString("popis")));
		}
		return output;
	}

	public void updateSubject(String id, String name, String credits, String about) throws SQLException {
		String query = "UPDATE predmet SET meno='" + name + "', kredity='" + credits + "', popis='" + about + "' WHERE predmet.predmetID=" + id + ";";
		
		DatabaseConnection.getMe().makeDatabaseUpdate(query);
		
	}

	public boolean hasStudentSubject(String studentID, String subjectID) throws SQLException {
		String query = "SELECT zp.zapisany_predmetID "
				+ "FROM zapisany_predmet AS zp "
				+ "JOIN student AS s ON s.studentID = zp.studentID "
				+ "WHERE s.studentID =" + studentID + " AND zp.predmetID =" + subjectID + ";";
		
		ResultSet has = DatabaseConnection.getMe().makeDatabaseCall(query);
		if (has.next()) {
			return true;
		} else {
			return false;
		}
	}

	public ObservableList<String> getLectorSubjects(int ucitelID) throws SQLException {
		ObservableList<String> output = FXCollections.observableArrayList();
		
		String query = "SELECT p.predmetID, p.meno "
				+ "FROM predmet AS p "
				+ "JOIN prednasajuci AS pr ON p.prednasajuciID = pr.prednasajuciID "
				+ "WHERE pr.ucitelID =" + ucitelID + " ORDER BY p.meno";
		
		ResultSet subjects = DatabaseConnection.getMe().makeDatabaseCall(query);
		
		while (subjects.next()) {
			output.add(subjects.getInt("predmetID") + " - " + subjects.getString("meno"));
		}
		
		return output;
	}



	
}
