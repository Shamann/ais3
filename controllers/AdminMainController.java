package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import user.UserInfo;
import database.DatabaseConnection;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class AdminMainController implements Initializable{

	@FXML
	public GridPane adminPane;
	@FXML
	public Label nameLabel;
	@FXML
	public Label statusLabel;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			adminPane.getChildren().setAll((GridPane) (FXMLLoader.load(getClass().getResource("/views/ManageStudentView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
		nameLabel.setText("You are logged in as: "
				+ UserInfo.getUser().getName() + " "
				+ UserInfo.getUser().getSurname() + ", ID: "
				+ UserInfo.getUser().getUserID());
	}

	@FXML
	private void manageStudents() {
		try {
			// TODO add multiple insert from file
			statusLabel.setText("Student management");
			adminPane.getChildren().setAll((GridPane) (FXMLLoader.load(getClass().getResource("/views/ManageStudentView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void manageSubjects() {
		try {
			statusLabel.setText("Student subjects management");
			adminPane.getChildren().setAll((GridPane) (FXMLLoader.load(getClass().getResource("/views/ManageSubjectView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void manageTeachers() {
		try {
			statusLabel.setText("Teacher management");
			adminPane.getChildren().setAll((GridPane) (FXMLLoader.load(getClass().getResource("/views/ManageTeacherView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void logOut() {
		DatabaseConnection.getMe().close();
		System.exit(0);
	}
	
}
