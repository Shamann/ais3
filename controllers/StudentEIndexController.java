package controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import user.UserInfo;
import models.SubjectManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

public class StudentEIndexController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			subjectList.setItems(sm.getStudentSubjectStats("" + UserInfo.getUser().getUserID()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	@FXML
	private ListView<String> subjectList;
	
	private SubjectManager sm = new SubjectManager();
}
