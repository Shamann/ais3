package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import database.DatabaseConnection;
import user.UserInfo;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;

public class MainController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		personalAreaBtn.setText("Personal Area of\n" + UserInfo.getUser().getName() + " " + UserInfo.getUser().getSurname());
	}

	@FXML
	private Button personalAreaBtn;
	@FXML
	private Button logoutBtn;
	@FXML
	AnchorPane mainViewAnchor;
	
	
	@FXML
	private void loadPA() {
		try {
			mainViewAnchor.getChildren().setAll((AnchorPane) (FXMLLoader.load(getClass().getResource("/views/PersonalAreaView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void showSettings() {
		
	}
	
	@FXML
	private void logOut() {
		DatabaseConnection.getMe().close();
		System.exit(0);
	}
}
