package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import user.UserInfo;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class PersonalAreaController implements Initializable {

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			innerModel.getChildren().setAll((AnchorPane) (FXMLLoader.load(getClass().getResource("/views/StudentStudyDetailsView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
		nameLbl.setText("You are logged in as: "
				+ UserInfo.getUser().getName() + " "
				+ UserInfo.getUser().getSurname() + ", ID: "
				+ UserInfo.getUser().getUserID());
	}
	
	@FXML
	private AnchorPane innerModel;
	@FXML
	private AnchorPane personalViewAnchor;
	@FXML
	private Label nameLbl;
	
	@FXML
	public void showEIndex() {
		try {
			innerModel.getChildren().setAll((AnchorPane) (FXMLLoader.load(getClass().getResource("/views/StudentEIndexView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void showSchoolmates() {

	}
	
	@FXML
	public void showStudyDetails() {
		try {
			innerModel.getChildren().setAll((AnchorPane) (FXMLLoader.load(getClass().getResource("/views/StudentStudyDetailsView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void showStudyPlan() {
		try {
			innerModel.getChildren().setAll((AnchorPane) (FXMLLoader.load(getClass().getResource("/views/StudentSubjectManagementView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	public void showLectureSheet() {

	}
	
	@FXML
	public void toMainView() {
		try {
			personalViewAnchor.getChildren().setAll((AnchorPane) (FXMLLoader.load(getClass().getResource("/views/MainView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
