package controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import database.DatabaseConnection;
import models.SubjectManager;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyEvent;

public class ManageSubjectController implements Initializable{


	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		studentListLv.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				fillSubjectLV();
				fillStudentSubjects(studentListLv.getSelectionModel().getSelectedItem());
				fillAdder(studentListLv.getSelectionModel().getSelectedItem());
			}
		});
		
		allSubjectsLv.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				subjectAddFld.setText(allSubjectsLv.getSelectionModel().getSelectedItem());
				try {
					String studentID = studentIDAddFld.getText();
					String subjectID = allSubjectsLv.getSelectionModel().getSelectedItem().split(" ")[0];
					if (subjectManager.hasStudentSubject(studentID, subjectID)) {
						applyAddBtn.setDisable(true);
					} else {
						applyAddBtn.setDisable(false);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			}
		});
		
		studentSubjectLv.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> arg0,	String arg1, String arg2) {
				if (studentListLv.getSelectionModel().getSelectedItem()!=null) {
					fillDeleter(studentListLv.getSelectionModel().getSelectedItem());
					subjectDelFld.setText(studentSubjectLv.getSelectionModel().getSelectedItem());
				}
			}
			
		});
		
	}
	
	protected void fillDeleter(String selectedItem) {
		String[] tmp = selectedItem.split(" ");
		studentIDDelFld.setText(tmp[0]);
		studentNameDelFld.setText(tmp[2] + " " + tmp[3]);
	}

	protected void fillAdder(String selectedItem) {
		String[] tmp = selectedItem.split(" ");
		studentIDAddFld.setText(tmp[0]);
		studentNameAddFld.setText(tmp[2] + " " + tmp[3]);
	}

	private void fillSubjectLV() {
		final Task<Void> showSubjcts = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				otherSubjects = subjectManager.getAllSubjectsForStudent();
				return null;
			};
		};
		showSubjcts.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				allSubjectsLv.setItems(otherSubjects);
			}
		});
		showSubjcts.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				showSubjcts.getException().printStackTrace();
			}
		});
		new Thread(showSubjcts).start();
	}

	private void fillStudentSubjects(final String studentID) {
		
		final Task<Void> showStudentSubjects = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				studentSubjects = subjectManager.getStudentSubjects(studentID);
				return null;
			};
			
		};
		showStudentSubjects.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent event) {
				studentSubjectLv.setItems(studentSubjects);
			}
		});
		showStudentSubjects.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				showStudentSubjects.getException().printStackTrace();
				
			}
		});
		new Thread(showStudentSubjects).start();
		
	}

	@FXML
	private TextField searchStudentFld;
	@FXML
	private TextField studentIDAddFld;
	@FXML
	private TextField studentNameAddFld;
	@FXML
	private TextField subjectAddFld;
	@FXML
	private TextField studentIDDelFld;
	@FXML
	private TextField studentNameDelFld;
	@FXML
	private TextField subjectDelFld;
	@FXML
	private TextField yearFld;
	@FXML
	private TextField semestFld;
	@FXML
	private Label successDelLbl;
	@FXML
	private Label successInsLbl;
	@FXML
	private TitledPane x7;
	@FXML
	private TitledPane x1;
	@FXML
	private Button applyAddBtn;
	@FXML
	private ListView<String> studentSubjectLv;
	@FXML
	private ListView<String> allSubjectsLv;
	@FXML
	private ListView<String> studentListLv;
	private ObservableList<String> foundStudents;
	private ObservableList<String> studentSubjects;
	private ObservableList<String> otherSubjects;
	
	private SubjectManager subjectManager = new SubjectManager();
	
	@FXML
	private void successAoff() {
		successInsLbl.setVisible(false);
	}
	@FXML
	private void successDoff() {
		successDelLbl.setVisible(false);
	}
	
	@FXML
	public void applyInsert(ActionEvent event) {
		try {
			if (subjectAddFld.getText() != null) {
				subjectManager.addSubject(studentIDAddFld.getText(), subjectAddFld.getText().split(" ")[0], yearFld.getText(), semestFld.getText());
				fillStudentSubjects(studentIDAddFld.getText());
				successInsLbl.setVisible(true);
			}
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
	}
	
	@FXML
	public void cancelInsert(ActionEvent event) {
		studentIDAddFld.clear();
		studentNameDelFld.clear();
		subjectDelFld.clear();
	}
	
	@FXML
	public void applyDelete(ActionEvent event) {
		try {
			if (subjectDelFld.getText() != null) {
				subjectManager.deleteSubject(studentIDDelFld.getText(), subjectDelFld.getText().split(" ")[0]);
				fillStudentSubjects(studentIDDelFld.getText());
				successDelLbl.setVisible(true);
			}
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	
	@FXML
	public void cancelDelete(ActionEvent event) {
		studentIDDelFld.clear();
		studentNameDelFld.clear();
		subjectDelFld.clear();
	}
	
	@FXML
	public void searchStudents(KeyEvent event) {
		final Task<Void> startSearch = new Task<Void>() {

			@Override
			protected Void call() throws Exception {
				String name = searchStudentFld.getText();
				foundStudents = subjectManager.getStudents(name);
				return null;
			};
			
		};
		startSearch.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			@Override
			public void handle(WorkerStateEvent event) {
				studentListLv.setItems(foundStudents);
			}
		});
		startSearch.setOnFailed(new EventHandler<WorkerStateEvent>() {

			@Override
			public void handle(WorkerStateEvent event) {
				startSearch.getException().printStackTrace();
				
			}
		});
		new Thread(startSearch).start();
	}

}
