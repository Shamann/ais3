package controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import database.Configuration;
import user.UserInfo;
import models.Login;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class LoginController implements Initializable {

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	}
	
	@FXML
	AnchorPane loginAnchor;
	
	@FXML
	private Label wrongLogin;
	@FXML
	private TextField nameFld;
	@FXML
	private TextField passFld;
	
	
	public void logIn() {
		Login loginVerifier = new Login();	
		String name = "";
		String passwd = "";
		
		name = nameFld.getText();
		passwd = passFld.getText();
		
		if (loginVerifier.verifyLogin(name, passwd)) {
		try {
				UserInfo.getUser().setLogin(name);
				if (UserInfo.getUser().getUserID() >= Configuration.ucitel) {
					loginAnchor.getChildren().setAll((AnchorPane) (FXMLLoader.load(getClass().getResource("/views/TeacherMainView.fxml"))));
					wrongLogin.setVisible(false);
				} else 
				if (UserInfo.getUser().getUserID() >= Configuration.admin) {
					loginAnchor.getChildren().setAll((AnchorPane) (FXMLLoader.load(getClass().getResource("/views/AdminMainView.fxml"))));
					wrongLogin.setVisible(false);
				} else 
				if (UserInfo.getUser().getUserID() >= Configuration.student) {
					loginAnchor.getChildren().setAll((AnchorPane) (FXMLLoader.load(getClass().getResource("/views/MainView.fxml"))));
					wrongLogin.setVisible(false);
				} 
				
			} catch (IOException | SQLException e) {
				e.printStackTrace();
			}
		} else {
			wrongLogin.setVisible(true);
			passFld.clear();
		}
	}

}
