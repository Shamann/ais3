package controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import database.DatabaseConnection;
import user.UserInfo;
import models.SubjectManager;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

public class StudentSubjectManagementController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fillSubjectLV();
		fillStudentSubjects();
		
		allSubjectsLv.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (allSubjectsLv.getSelectionModel().getSelectedItem() != null) {
					try {
						successInsLbl.setVisible(false);
						if(subjectManager.hasStudentSubject(UserInfo.getUser().getUserID() +"" , allSubjectsLv.getSelectionModel().getSelectedItem().split(" ")[0])) {
							applyAddBtn.setDisable(true);
						} else {
							applyAddBtn.setDisable(false);
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	
	@FXML
	private ListView<String> studentSubjectLv;
	@FXML
	private ListView<String> allSubjectsLv;
	@FXML
	private Button applyAddBtn;
	@FXML
	private Label successInsLbl;
	
	@FXML
	private void applyInsert() {
		try {
			subjectManager.addSubject(UserInfo.getUser().getUserID()+"", allSubjectsLv.getSelectionModel().getSelectedItem().split(" ")[0], "1", "1");
			fillStudentSubjects();
			successInsLbl.setVisible(true);
		
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
	}
	
	SubjectManager subjectManager = new SubjectManager();
	
	private void fillSubjectLV() {
		final Task<Void> showSubjcts = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				allSubjectsLv.setItems(subjectManager.getAllSubjectsForStudent());
				return null;
			};
		};
		new Thread(showSubjcts).start();
	}
	
	private void fillStudentSubjects() {
			
			final Task<Void> showStudentSubjects = new Task<Void>() {
	
				@Override
				protected Void call() throws Exception {
					studentSubjectLv.setItems(subjectManager.getStudentSubjects(UserInfo.getUser().getUserID()+""));
					return null;
				};
				
			};
			new Thread(showStudentSubjects).start();
			
		}
}
