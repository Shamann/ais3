package controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import models.SubjectManager;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

public class SubjectStatisticsController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			subjectLv.setItems(sm.getSubjectStatistics());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	private SubjectManager sm = new SubjectManager();
	@FXML
	private ListView<String> subjectLv;
}
