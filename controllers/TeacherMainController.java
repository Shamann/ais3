package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import user.UserInfo;
import database.DatabaseConnection;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class TeacherMainController implements Initializable {

	@FXML
	public GridPane teacherPane;
	@FXML
	public Label nameLabel;
	@FXML
	public Label statusLabel;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			teacherPane.getChildren().setAll((GridPane) (FXMLLoader.load(getClass().getResource("/views/StudiesManagementView.fxml"))));
			nameLabel.setText("You are logged in as: "
					+ UserInfo.getUser().getName() + " "
					+ UserInfo.getUser().getSurname() + ", ID: "
					+ UserInfo.getUser().getUserID());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void studiesManagement() {
		try {
			statusLabel.setText("Studies management");
			teacherPane.getChildren().setAll((GridPane) (FXMLLoader.load(getClass().getResource("/views/StudiesManagementView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void manageSubjects() {
		try {
			statusLabel.setText("Lector management");
			teacherPane.getChildren().setAll((GridPane) (FXMLLoader.load(getClass().getResource("/views/LectorManagementView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void subjectStats() {
		try {
			statusLabel.setText("Subject statistics");
			teacherPane.getChildren().setAll((GridPane) (FXMLLoader.load(getClass().getResource("/views/SubjectStatisticsView.fxml"))));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void logOut() {
		DatabaseConnection.getMe().close();
		System.exit(0);
	}

}
