package controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import dataObjects.TeacherTable;
import database.DatabaseConnection;
import models.ManageTeachers;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;

public class ManageTeacherController implements Initializable {
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		teacherTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TeacherTable>() {
			@Override
			public void changed(ObservableValue<? extends TeacherTable> observable, TeacherTable oldValue, TeacherTable newValue) {
				setFields();
			}       
		});
		fillTeacherTable();
	}
	
	private void setFields() {
		if (teacherTable.getSelectionModel().getSelectedItem() != null) {
			nameUpd.setText(teacherTable.getSelectionModel().getSelectedItem().getName());
			nameDel.setText(teacherTable.getSelectionModel().getSelectedItem().getName());
			surnameUpd.setText(teacherTable.getSelectionModel().getSelectedItem().getSurname());
			surnameDel.setText(teacherTable.getSelectionModel().getSelectedItem().getSurname());
			teacherIdDel.setText(teacherTable.getSelectionModel().getSelectedItem().getIdentificator() + "");
			teacherSinceUpd.setText(teacherTable.getSelectionModel().getSelectedItem().getTeachSince());
			mailUpd.setText(teacherTable.getSelectionModel().getSelectedItem().getEmail());
		}
	}

	@FXML
	private TextField nameAdd;
	@FXML
	private TextField surnameAdd;
	@FXML
	private TextField teacherSinceAdd;
	@FXML
	private TextField mailAdd;
	@FXML
	private TextField nameUpd;
	@FXML
	private TextField surnameUpd;
	@FXML
	private TextField teacherSinceUpd;
	@FXML
	private TextField mailUpd;
	@FXML
	private TextField newPasswd;
	@FXML
	private TextField nameDel;
	@FXML
	private TextField surnameDel;
	@FXML
	private TextField teacherIdDel;
	@FXML
	private Label successLbl;
	@FXML
	private Label passLbl;
	@FXML
	private TitledPane x3;
	
	@FXML
	private void invisibleSucces(){
		successLbl.setVisible(false);
		passLbl.setVisible(false);
	}
	
	@FXML
	private void addTeacher(ActionEvent event) {
		try {
			String passwd = teacherManager.insertTeacher(nameAdd.getText(), surnameAdd.getText(), teacherSinceAdd.getText() , mailAdd.getText());
			clearAdd();
			fillTeacherTable();
			passLbl.setText(passwd);
			passLbl.setVisible(true);
			
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	
	@FXML
	private void updateTeacher(ActionEvent event) {
		try {
			if (teacherTable.getSelectionModel().getSelectedItem() != null) {
				teacherManager.updateTeacher(teacherTable.getSelectionModel().getSelectedItem().getIdentificator(), 
						nameUpd.getText(), 
						surnameUpd.getText(), 
						teacherSinceUpd.getText(), 
						mailUpd.getText(), 
						newPasswd.getText());
				fillTeacherTable();
				successLbl.setText("Update Successful!");
				successLbl.setVisible(true);
			}
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
		
	}
	
	@FXML
	private void deleteTeacher(ActionEvent event) {
		try {
			teacherManager.deleteTeacher(teacherTable.getSelectionModel().getSelectedItem().getName(), 
					teacherTable.getSelectionModel().getSelectedItem().getSurname(), 
					teacherTable.getSelectionModel().getSelectedItem().getIdentificator());
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		nameDel.clear();
		surnameDel.clear();
		teacherIdDel.clear();
	}

	@FXML
	private void clearAdd(){
		nameAdd.clear();
		surnameAdd.clear();
		teacherSinceAdd.clear();
		mailAdd.clear();
	}
	@FXML
	private void cancelAdd(ActionEvent event) {
		clearAdd();
	}
	
	@FXML
	private void cancelUpdate(ActionEvent event) {
		setFields();
	}
	
	@FXML
	private void cancelDelete(ActionEvent event) {
		nameDel.clear();
		surnameDel.clear();
		teacherIdDel.clear();
	}

	@FXML
	private void confirmCancel() {
		// TODO confirm cancel teacher
	}
	
	@FXML
	private void denyCancel() {
		// TODO deny cancel teacher
	}
	
	@FXML
	public TableView<TeacherTable> teacherTable;
	@FXML
	public TableColumn<TeacherTable, Integer> teacherID;
	@FXML
	public TableColumn<TeacherTable, String> nameCol;
	@FXML
	public TableColumn<TeacherTable, String> surnameCol;
	@FXML
	public TableColumn<TeacherTable, String> teacherSinceCol;
	@FXML
	public TableColumn<TeacherTable, String> emailCol;
	
	
	private ObservableList<TeacherTable> teacherList = FXCollections.observableArrayList();
	private ManageTeachers teacherManager = new ManageTeachers();
	
	private void fillTeacherTable() {
		teacherList.clear();
		try {
			teacherList = teacherManager.loadTeachersToTable();
			teacherTable.setItems(teacherList);
			teacherID.setCellValueFactory(TeacherTable.getIdentificatorProperty());
			nameCol.setCellValueFactory(TeacherTable.getNameProperty());
			surnameCol.setCellValueFactory(TeacherTable.getSurnameNameProperty());
			teacherSinceCol.setCellValueFactory(TeacherTable.getTeachSinceProperty());
			emailCol.setCellValueFactory(TeacherTable.getMailProperty());
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
