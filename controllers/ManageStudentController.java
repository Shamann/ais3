package controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import dataObjects.StudentTable;
import database.DatabaseConnection;
import models.ManageStudents;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
public class ManageStudentController implements Initializable {
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		studentTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<StudentTable>() {
			@Override
			public void changed(ObservableValue<? extends StudentTable> observable, StudentTable oldValue, StudentTable newValue) {
				setFields();
			}       
		});
		fillStudentTable();
	}
	
	private void setFields() {
		if (studentTable.getSelectionModel().getSelectedItem() != null) {
			nameUpd.setText(studentTable.getSelectionModel().getSelectedItem().getName());
			nameDel.setText(studentTable.getSelectionModel().getSelectedItem().getName());
			surnameUpd.setText(studentTable.getSelectionModel().getSelectedItem().getSurname());
			surnameDel.setText(studentTable.getSelectionModel().getSelectedItem().getSurname());
			birthNumUpd.setText(studentTable.getSelectionModel().getSelectedItem().getBirthNum());
			birthNumDel.setText(studentTable.getSelectionModel().getSelectedItem().getBirthNum());
			birthDateUpd.setText(studentTable.getSelectionModel().getSelectedItem().getBirthday());
			semesterUpd.setText(studentTable.getSelectionModel().getSelectedItem().getPeriod() + "");
			yearUpd.setText(studentTable.getSelectionModel().getSelectedItem().getYear() + "");
			facultyUpd.setText(studentTable.getSelectionModel().getSelectedItem().getFaculty());
			mailUpd.setText(studentTable.getSelectionModel().getSelectedItem().getEmail());
		}
	}

	@FXML
	private TextField nameAdd;
	@FXML
	private TextField surnameAdd;
	@FXML
	private TextField birthNumAdd;
	@FXML
	private TextField birthDateAdd;
	@FXML
	private TextField semesterAdd;
	@FXML
	private TextField yearAdd;
	@FXML
	private TextField facultyAdd;
	@FXML
	private TextField mailAdd;
	@FXML
	private TextField nameUpd;
	@FXML
	private TextField surnameUpd;
	@FXML
	private TextField birthNumUpd;
	@FXML
	private TextField birthDateUpd;
	@FXML
	private TextField semesterUpd;
	@FXML
	private TextField yearUpd;
	@FXML
	private TextField facultyUpd;
	@FXML
	private TextField mailUpd;
	@FXML
	private TextField newPasswd;
	@FXML
	private TextField nameDel;
	@FXML
	private TextField surnameDel;
	@FXML
	private TextField birthNumDel;
	@FXML
	private Label successLbl;
	@FXML
	private TitledPane x3;
	
	@FXML
	private void invisibleSucces(){
		successLbl.setVisible(false);
	}
	@FXML
	private void addStudent(ActionEvent event) {
		try {
			studentManager.insertStudent(nameAdd.getText(),
					surnameAdd.getText(), birthNumAdd.getText(),
					birthDateAdd.getText(), semesterAdd.getText(),
					yearAdd.getText(), facultyAdd.getText(), mailAdd.getText());
			clearAdd();
			fillStudentTable();
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	
	@FXML
	private void updateStudent(ActionEvent event) {
		try {
			studentManager.updateStudent(studentTable.getSelectionModel().getSelectedItem().getIdentificator(), nameUpd.getText(),
						surnameUpd.getText(), birthNumUpd.getText(),
						birthDateUpd.getText(), semesterUpd.getText(),
						yearUpd.getText(), facultyUpd.getText(), mailUpd.getText(), newPasswd.getText());
			fillStudentTable();
			successLbl.setVisible(true);
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
		
	}
	
	@FXML
	private void deleteStudent(ActionEvent event) {
		try {
			studentManager.deleteStudent(
					studentTable.getSelectionModel().getSelectedItem().getName(), 
					studentTable.getSelectionModel().getSelectedItem().getSurname(),
					studentTable.getSelectionModel().getSelectedItem().getBirthNum());
			fillStudentTable();
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		nameDel.clear();
		surnameDel.clear();
		birthNumDel.clear();
	}

	@FXML
	private void clearAdd(){
		nameAdd.clear();
		surnameAdd.clear();
		birthNumAdd.clear();
		birthDateAdd.clear();
		yearAdd.clear();
		facultyAdd.clear();
		semesterAdd.clear();
		mailAdd.clear();
	}
	@FXML
	private void cancelAdd(ActionEvent event) {
		clearAdd();
	}
	
	@FXML
	private void cancelUpdate(ActionEvent event) {
		setFields();
	}
	
	@FXML
	private void cancelDelete(ActionEvent event) {
		nameDel.clear();
		surnameDel.clear();
		birthNumDel.clear();
	}

	@FXML
	private void confirmCancel() {
		// TODO confirm cancel
	}
	
	@FXML
	private void denyCancel() {
		// TODO deny cancel
	}
	
	@FXML
	public TableView<StudentTable> studentTable;
	@FXML
	public TableColumn<StudentTable, Integer> studentID;
	@FXML
	public TableColumn<StudentTable, String> nameCol;
	@FXML
	public TableColumn<StudentTable, String> surnameCol;
	@FXML
	public TableColumn<StudentTable, String> birthNumCol;
	@FXML
	public TableColumn<StudentTable, String> birthdayCol;
	@FXML
	public TableColumn<StudentTable, Integer> periodCol;
	@FXML
	public TableColumn<StudentTable, Integer> yearCol;
	@FXML
	public TableColumn<StudentTable, String> facultyCol;
	@FXML
	public TableColumn<StudentTable, String> emailCol;
	
	
	private ObservableList<StudentTable> studentList = FXCollections.observableArrayList();
	private ManageStudents studentManager = new ManageStudents();
	
	private void fillStudentTable() {
		studentList.clear();
		try {
			studentList = studentManager.loadStudentsToTable();
			studentTable.setItems(studentList);
			studentID.setCellValueFactory(StudentTable.getIdentificatorProperty());
			nameCol.setCellValueFactory(StudentTable.getNameProperty());
			surnameCol.setCellValueFactory(StudentTable.getSurnameNameProperty());
			birthNumCol.setCellValueFactory(StudentTable.getBirthNumProperty());
			birthdayCol.setCellValueFactory(StudentTable.getBirthDayProperty());
			periodCol.setCellValueFactory(StudentTable.getPeriodProperty());
			yearCol.setCellValueFactory(StudentTable.getYearProperty());
			facultyCol.setCellValueFactory(StudentTable.getFacultyProperty());
			emailCol.setCellValueFactory(StudentTable.getMailProperty());
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
