package controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import user.UserInfo;
import models.ManageTeachers;
import models.SubjectManager;
import dataObjects.SubjectList;
import database.DatabaseConnection;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class LectorManagementController implements Initializable{

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		fillSubjects();
		subjectTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<SubjectList>() {

			@Override
			public void changed(ObservableValue<? extends SubjectList> observable, SubjectList oldValue, SubjectList newValue) {
				fillUpdates(subjectTable.getSelectionModel().getSelectedItem());
				succAddLbl.setVisible(false);
				succUpdLbl.setVisible(false);
			}
		});
	}

	private void fillUpdates(SubjectList selectedItem) {
		idUpd.setText(selectedItem.getSubjectID() + "");
		nameUpd.setText(selectedItem.getName());
		creditsUpd.setText(selectedItem.getCredits() + "");
		lectorUpd.setText(selectedItem.getLectorName());
		aboutUpd.setText(selectedItem.getText());
		
		if (selectedItem.getLector() == -1) {
			dropUpd.setDisable(true);
			becomeUpd.setDisable(false);
			applyUpd.setDisable(false);
		} else 
		if (selectedItem.getLector() != UserInfo.getUser().getUserID()) {
			dropUpd.setDisable(true);
			becomeUpd.setDisable(true);
			applyUpd.setDisable(true);
			
		} else
		if (selectedItem.getLector() == UserInfo.getUser().getUserID()) {
			dropUpd.setDisable(false);
			becomeUpd.setDisable(true);
			applyUpd.setDisable(false);
			
		}
	}

	@FXML
	private Label succUpdLbl;
	@FXML
	private Label succAddLbl;
	@FXML
	private TextField idUpd;
	@FXML
	private TextField nameUpd;
	@FXML
	private TextField creditsUpd;
	@FXML
	private TextField lectorUpd;
	@FXML
	private TextArea aboutUpd;
	@FXML
	private Button becomeUpd;
	@FXML
	private Button dropUpd;
	@FXML
	private Button applyUpd;
	@FXML
	private Button createBtn;
	@FXML
	private TextField nameAdd;
	@FXML
	private TextField creditsAdd;
	@FXML
	private TextArea aboutAdd;
	@FXML
	private TableView<SubjectList> subjectTable;
	@FXML
	private TableColumn<SubjectList, Integer> idCol;
	@FXML
	private TableColumn<SubjectList, String> nameCol;
	@FXML
	private TableColumn<SubjectList, Integer> creditsCol;
	@FXML
	private TableColumn<SubjectList, String> lectorCol;
	@FXML
	private TableColumn<SubjectList, String> aboutCol;
	
	private ObservableList<SubjectList> subjects = FXCollections.observableArrayList();
	
	private SubjectManager subjectManager = new SubjectManager();
	private ManageTeachers teacherManager = new ManageTeachers();
	
	private void fillSubjects() {
		try {
			subjects = subjectManager.getAllSubjects();
			subjectTable.setItems(subjects);
			fillColumns();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	private void fillColumns() {
		idCol.setCellValueFactory(SubjectList.getIDproperty());
		nameCol.setCellValueFactory(SubjectList.getNameProperty());
		creditsCol.setCellValueFactory(SubjectList.getCreditsProperty());
		lectorCol.setCellValueFactory(SubjectList.getLectorProperty());
		aboutCol.setCellValueFactory(SubjectList.getAboutProperty());
	}

	@FXML
	private void becomeLector(ActionEvent event) {
		try {
			teacherManager.becomeLector(idUpd.getText(), UserInfo.getUser().getUserID());
			succUpdLbl.setVisible(true);
//FIXME			fillSubjects();
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	
	@FXML
	private void dropLectoring(ActionEvent event) {
		try {
			teacherManager.dropLectoring(idUpd.getText());
			succUpdLbl.setVisible(true);
//FIXME			fillSubjects();
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
	}
	
	@FXML
	private void createSubject(ActionEvent event) {
		try {
			teacherManager.createNewSubject(nameAdd.getText(), creditsAdd.getText(), aboutAdd.getText(), UserInfo.getUser().getUserID());
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	
	@FXML
	private void stashSuccesUpd() {
		succUpdLbl.setVisible(false);
	}
	@FXML
	private void stashsuccessCreate() {
		succAddLbl.setVisible(false);
	}
	
	@FXML
	private void applyUpd() {
		try {
			subjectManager.updateSubject(idUpd.getText(), nameUpd.getText(), creditsUpd.getText(), aboutUpd.getText());
			succUpdLbl.setVisible(true);
//FIXME			fillSubjects();
		} catch (SQLException e) {
			try {
				DatabaseConnection.getMe().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	
	@FXML
	private void stashUpd() {
		idUpd.clear();
		nameUpd.clear();
		creditsUpd.clear();
		lectorUpd.clear();
		aboutUpd.clear();
		becomeUpd.setDisable(true);
		dropUpd.setDisable(true);
		applyUpd.setDisable(true);
	}
	
	@FXML
	private void stashAdd(ActionEvent event) {
		nameAdd.clear();
		creditsAdd.clear();
		aboutAdd.clear();
		createBtn.setDisable(true);
	}
}
