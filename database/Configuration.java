package database;

import java.util.Random;

public class Configuration {
	
	public static final int student = 10000;
	public static final int ucitel = 30000;
	public static final int admin = 20000;
	
	private static final String url = "jdbc:mysql://localhost:3306/";
	private static final String dbName = "ais3?characterEncoding=UTF-8";
	private static final String driver = "com.mysql.jdbc.Driver";
	private static final String userName = "Lukas";
	private static final String password = "as23c5";
	
	public static String getUrl() {
		return url;
	}
	public static String getDbname() {
		return dbName;
	}
	public static String getDriver() {
		return driver;
	}
	public static String getUsername() {
		return userName;
	}
	public static String getPassword() {
		return password;
	}
	
	/**
	 * Method from stackoverflow WhiteFang34
	 * http://stackoverflow.com/questions/5683327/how-to-generate-a-random-string-of-20-characters
	 * @return new Random password
	 */
	public static String getRandomPass() {
		char[] chars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMOPQRSTUVWXYZ".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 8; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		String output = sb.toString();
		return output;
	}
	
}
