package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DatabaseConnection {

	private Connection conn;
	
	private static final DatabaseConnection dbconn = new DatabaseConnection();
	
	private DatabaseConnection() {
		String driver = Configuration.getDriver();
		String database = Configuration.getUrl() + Configuration.getDbname();
		
		
		try {
			Class.forName(driver).newInstance();
			
			conn = DriverManager.getConnection(database, Configuration.getUsername(), Configuration.getPassword());
			
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	public static DatabaseConnection getMe() {
		return dbconn;
	}
	
	public Connection getConnection() {
		return conn;
	}
	
	public void close() {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * makes database call and returns results
	 * @param query String
	 * @return ResultSet - result of sent query
	 * @throws SQLException
	 */
	public ResultSet makeDatabaseCall(String query) throws SQLException {
		
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		
		return rs;
	}
	
	public ResultSet makeDatabaseUpdate(String query) throws SQLException {
		
		Statement stmt = conn.createStatement();
		
		DatabaseConnection.getMe().getConnection().setAutoCommit(false);
		stmt.executeUpdate(query);
		ResultSet res = stmt.getResultSet();
		DatabaseConnection.getMe().getConnection().commit();
		return res;
	}
	

}
