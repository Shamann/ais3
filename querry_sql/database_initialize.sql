ALTER DATABASE ais3 CHARACTER SET utf8 COLLATE utf8_general_ci;
use ais3;

create table MailBox (
	MailBoxID int not null auto_increment,
	primary key (MailBoxID)
) ;

create table student (
	studentID int not null auto_increment,
	login VARCHAR(20),
	passwd varchar(20),
	Meno varchar(30),
	Priezvisko varchar(30),
	Rodne_c varchar(11),
	dat_nar date,
	semester int,
	rocnik int,
	ustav varchar(50),
	mail VARCHAR(50),
	MailBoxID int,
	primary key (studentID),
	foreign key (MailBoxID) references MailBox(MailBoxID) on update cascade on delete set null
) ;
ALTER TABLE student AUTO_INCREMENT = 10000;

create table administrator (
	administratorID int not null auto_increment,
	login VARCHAR(20),
	passwd varchar(20),
	Meno varchar(30),
	Priezvisko varchar(30),
	mail VARCHAR(50),
	MailBoxID int,
	primary key (administratorID),
	foreign key (MailBoxID) references MailBox(MailBoxID) on update cascade on delete set null
) ;
ALTER TABLE administrator AUTO_INCREMENT = 20000;

create table ucitel (
	ucitelID int not null auto_increment,
	MailBoxID int,
	login VARCHAR(20) not null,
	passwd varchar(20) not null,
	Meno varchar(30) not null,
	Priezvisko varchar(30) not null,
	od_dna date,
	mail VARCHAR(50),
	primary key (ucitelID),
	foreign key (MailBoxID) references MailBox(MailBoxID) on update cascade on delete set null
) AUTO_INCREMENT = 30000;

create table prednasajuci(
	prednasajuciID int not null auto_increment,
	od_dna date,
	ucitelID int null,
	primary key (prednasajuciID),
	foreign key (ucitelID) references ucitel(ucitelID) on update cascade on delete cascade
) ;

create table cviciaci(
	cviciaciID int not null auto_increment,
	od_dna date,
	do_dna date,
	ucitelID int null,
	primary key (cviciaciID),
	foreign key (ucitelID) references ucitel(ucitelID) on update cascade on delete cascade
) ;

create table predmet (
	predmetID int not null auto_increment,
	meno varchar(60),
	kredity int,
	popis varchar(1500),
	prednasajuciID int,
	primary key (predmetID),
	foreign key (prednasajuciID) references prednasajuci(prednasajuciID) on update cascade on delete set null
) ;

create table cvicenie(
	cvicenieID int not null auto_increment,
	cviciaciID int,
	predmetID int not null,
	cas time,
	den varchar(15),
	primary key (cvicenieID),
	foreign key (cviciaciID) references cviciaci(cviciaciID) on update cascade on delete set null,
	foreign key (predmetID) references predmet(predmetID) on update cascade on delete cascade
) ;

create table student_na_cvicenie (
	student_na_cvicenieID int not null auto_increment,
	studentID int,
	cvicenieID int,
	primary key (student_na_cvicenieID),
	foreign key (studentID) references student(studentID) on update cascade on delete set null,
	foreign key (cvicenieID) references cvicenie(cvicenieID) on update cascade on delete set null
) ;

create table cvicenie_v_den (
	cvicenie_v_denID int not null auto_increment,
	datum datetime,
	pritomny tinyint(1),
	student_na_cvicenieID int,
	primary key (cvicenie_v_denID),
	foreign key (student_na_cvicenieID) references student_na_cvicenie(student_na_cvicenieID) on update cascade on delete cascade
) ;

create table skuska (
	skuskaID int not null auto_increment,
	termin datetime,
	pocet_ludi_max int,
	prednasajuciID int,
	predmetID int,
	primary key (skuskaID),
	foreign key (prednasajuciID) references prednasajuci(prednasajuciID) on update cascade on delete set null,
	foreign key (predmetID) references predmet(predmetID) on update cascade on delete set null
) ;

create table student_na_skuske (
	student_na_skuskeID int not null auto_increment,
	studentID int,
	skuskaID int,
	poradie int,
	body int null default '0',
	primary key (student_na_skuskeID),
	foreign key (studentID) references student(studentID) on update cascade on delete set null,
	foreign key (skuskaID) references skuska(skuskaID) on update cascade on delete set null
) ;

create table zapisany_predmet (
	zapisany_predmetID int not null auto_increment,
	semester int,
	rok int,
	body_zapocet int null default 0,
	spravil_predmet tinyint(1) not null default false,
	studentID int,
	predmetID int,
	primary key (zapisany_predmetID),
	foreign key (studentID) references student(studentID) on update cascade on delete cascade,
	foreign key (predmetID) references predmet(predmetID) on update cascade on delete cascade
) ;

create table zapoctovka (
	zapoctovkaID int not null auto_increment,
	poradie int,
	body int,
	termin datetime,
	predmetID int not null,
	primary key (zapoctovkaID),
	foreign key (predmetID) references zapisany_predmet(zapisany_predmetID) on update cascade on delete set null
);

