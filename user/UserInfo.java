package user;

import java.sql.ResultSet;
import java.sql.SQLException;

import database.DatabaseConnection;

public class UserInfo {
	
	private String name = "";
	private String surname = "";
	private String login = "";
	private int ID = -1;
	
	private UserInfo() {
	}
	
	private static final UserInfo user = new UserInfo();
	
	public static UserInfo getUser() {
		return user;
	}
	
	public String getName() {
		return name;
	}
	
	private void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	private void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getLogin() {
		return login;
	}
	
	public int getUserID() {
		return ID;
	}
	
	public void setLogin(String login) throws SQLException {
		this.login = login;

		String query = "SELECT studentID, meno, priezvisko	FROM student WHERE login = '"
				+ login
				+ "' UNION SELECT ucitelID, meno, priezvisko FROM ucitel  WHERE login = '"
				+ login
				+ "' UNION	SELECT administratorID, meno, priezvisko FROM administrator WHERE login = '"
				+ login + "';";
		ResultSet userResult = DatabaseConnection.getMe().makeDatabaseCall(query);
		if (userResult.next()) {
			String name = userResult.getString("meno");
			String surname = userResult.getString("priezvisko");
			int ID = userResult.getInt("studentID");
			setName(name);
			setSurname(surname);
			this.ID = ID;
			
		}
	}
}
